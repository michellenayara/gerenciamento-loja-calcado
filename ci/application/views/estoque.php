<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="/loja/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/loja/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/loja/ci/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/loja/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos">Loja de Calçados</a>
            </div>

             <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <!-- <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li> -->
                        <li class="divider"></li>
                        <li><a href="http://localhost/loja/ci/index.php/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <!-- /.navbar-top-links -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        
                        <br>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/adicionarproduto/add"><i class="fa fa-edit fa-fw"></i> Cadastrar Produto </a>
                        </li>
                        <li>
                          
                            <a href="http://localhost/loja/ci/index.php/paginaeditarproduto/paginaeditar" title="atualizar cadastro"><i class="fa fa fa-edit fa-fw"></i> Atualizar Estoque</a>
                            
                        </li>
                        <li>
                              <a href="http://localhost/loja/ci/index.php/paginaapagarproduto/paginaapagar"><i class="fa fa-edit fa-fw"></i> Apagar Produto</a>
                        </li>
                        <!--
                        <li>
                            <a href="http://localhost/monitoria/ci/index.php/monitoria/paginaFrequencia"><i class="fa fa-edit fa-fw"></i> Frequência</a>
                        </li>
                        -->
                        <li>
                            <a href="http://localhost/loja/ci/index.php/relatorioproduto/paginarelatorio"><i class="fa fa-edit fa-fw"></i> Relatório</a>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/vendaproduto/venda"><i class="fa fa-edit fa-fw"></i>Vendas </a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Estoque</h1>
                </div>

                 <!-- /.Inicio div Lista de produtos -->
                 

                <form action="estoquecompleto" method="post">
                    <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                            <h4>Produtos Cadastrados</h4>
                        </font></font></div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline collapsed" id=" dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 90%; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Código</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nome</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Preço de compra</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Preço de venda</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quantidade</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tipo</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Numeração</font></font></th>
                                        </tr>
                                    </thead>
                                     <?php

                              $contador = 0;
                              foreach ($itens as $produto)
                              {        
                                  echo '<tr>';
                                    echo '<td>'.$produto->id.'</td>';
                                    echo '<td>'.$produto->nome.'</td>';
                                    echo '<td>'.$produto->preco_compra.'</td>';
                                    echo '<td>'.$produto->preco_venda.'</td>'; 
                                    echo '<td>'.$produto->quantidade.'</td>'; 
                                    echo '<td>'.$produto->tipo.'</td>'; 
                                    echo '<td>'.$produto->tamanho.'</td>'; 
                                    
                                
                                    echo '</td>'; 
                                  echo '</tr>';
                              $contador++;
                              }
                          ?>
                                    
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>

                    <!-- /.Fim div Lista de produto -->
                </form> <!-- Fim do formulário para cadastrar -->

        </div>
        <!-- /#page-wrapper -->

    </div>

    <!-- jQuery -->
    <script src="/loja/ci/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/raphael/raphael.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/morrisjs/morris.min.js"></script>
    <script src="/loja/ci/bootstrap/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/loja/ci/bootstrap/dist/js/sb-admin-2.js"></script>

</body>

</html>
