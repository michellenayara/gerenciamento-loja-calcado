<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="Lista de produto da tabela produto">
    
    <title>Cadastrar produto</title>

    
    <link href="/loja/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
  </head>

  <body> 

      <div id="wrapper">
       
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos">Loja de Calçados</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li class="divider"></li>
                        <li><a href="http://localhost/loja/ci/index.php/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    
                </li>
               
            </ul>
          
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       <br>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/adicionarproduto/add"><i class="fa fa-edit fa-fw"></i> Cadastrar Produto</a>
                        </li>
                        <li>
                          
                            <a href="http://localhost/loja/ci/index.php/paginaeditarproduto/paginaeditar" title="atualizar cadastro"><i class="fa fa fa-edit fa-fw"></i> Atualizar Estoque</a>
                            
                        </li>
                        <li>
                              <a href="http://localhost/loja/ci/index.php/paginaapagarproduto/paginaapagar"><i class="fa fa-edit fa-fw"></i> Apagar Produto</a>
                        </li>
                        
                        <li>
                            <a href="http://localhost/loja/ci/index.php/relatorioproduto/paginaRelatorio"><i class="fa fa-edit fa-fw"></i> Relatórios</a>
                        </li>
                    </ul>
                </div>
                
            </div>
            
        </nav>
                
        <div id="page-wrapper">
          <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Novo Produto</h1>
            </div> 
           
            <form action="http://localhost/loja/ci/index.php/salvarproduto/salvar" name="form_add" method="post">
                           
              <div class="row">
                <div class="col-md-8">
                  <label>Nome</label>
                  <input type="text" name="nome" placeholder="Digite o nome da produto" value="" class="form-control">
                </div>
              </div>

              
              <div class="row">
                <div class="col-md-8">
                  <label>Preço de compra</label>
                  <input type="text" name="preco_compra" placeholder="Digite o preço de compra" value="" class="form-control">
                </div>
              </div>

               <div class="row">
                <div class="col-md-8">
                  <label>Preço de venda</label>
                  <input type="text" name="preco_venda" placeholder="Digite o preço de venda" value="" class="form-control">
                </div>
              </div>

               <div class="row">
                <div class="col-md-8">
                  <label>Quantidade</label>
                  <input type="text" name="quantidade" placeholder="Digite a quantidade do produto" value="" class="form-control">
                </div>
              </div>

              <div class="row">
                <div class="col-md-8">
                  <label>Tipo</label>
                  <input type="text" name="tipo" placeholder="Digite o tipo do produto" value="" class="form-control">
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <label>Numeração</label>
                  <input type="text" name="tamanho" placeholder="Digite a tamanho do produto" value="" class="form-control">
                </div>
              </div>
                         
              <input type="hidden" name="presenca" value="0">
              <input type="hidden" name="ausencia" value="0">

          
              <br />
              <div class="row">
                <div class="col-md-2">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
              
            </form>
          </div>
        </div>
      </div>

    <script src="../vendor/jquery/jquery.min.js"></script>

    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <script src="../vendor/raphael/raphael.min.js"></script>
    <script src="../vendor/morrisjs/morris.min.js"></script>
    <script src="../data/morris-data.js"></script>

    <script src="../dist/js/sb-admin-2.js"></script>

  </body>
</html>