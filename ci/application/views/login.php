<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login </title>

    <link href="/loja/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 align="center" class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                      
                        <form role="form" action="http://localhost/loja/ci/index.php/login/entrarlogin" name="form_login" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Login" name="login" type="text" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Senha" name="password" type="password" value="">
                                </div>
                        
                                      <button type="submit" class="btn btn-lg btn-success btn-block"">Entrar</button>
                             
                                </br>
                            </fieldset>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
