<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="Lista de produto da tabela produto">
    
    <title>Cadastrar produto</title>

    
    <link href="/loja/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
  </head>

  <body> 

      <div id="wrapper">
       
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos">Loja de Calçados</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li class="divider"></li>
                        <li><a href="http://localhost/loja/ci/index.php/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    
                </li>
               
            </ul>


             <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <br>
                        </li>
                       <li>
                            <a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/adicionarproduto/add"><i class="fa fa-edit fa-fw"></i> Cadastrar Produto </a>
                        </li>
                        <li>
                          
                            <a href="http://localhost/loja/ci/index.php/paginaeditarproduto/paginaeditar" title="atualizar cadastro"><i class="fa fa fa-edit fa-fw"></i> Atualizar Estoque</a>
                            
                        </li>
                        <li>
                              <a href="http://localhost/loja/ci/index.php/paginaapagarproduto/paginaapagar"><i class="fa fa-edit fa-fw"></i> Apagar Produto</a>
                        </li>
                       
                        <li>
                            <a href="http://localhost/loja/ci/index.php/relatorioproduto/paginaRelatorio"><i class="fa fa-edit fa-fw"></i> Relatórios</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
             


        <div id="page-wrapper">
          <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tela de Venda</h1>
        </div> 
      

       <form action="estoquecompleto" method="post">
                    <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                            <h4>Realizar venda</h4>
                        </font></font></div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline collapsed" id=" dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 90%; margin-left: 5%;">
                          
                          <thead>
                              <tr>
                                <!--<th class="text-center">Matricula</th> -->
                                <th class="text-center" style="width: 2%;">Código</th>
                                <th class="text-center" style="width: 10%;">Nome</th>
                                <!-- <th class="text-center" style="width: 10%;">Preço de compra</th> -->
                                <th class="text-center" style="width: 5%;">Quantidade</th>
                                 <th class="text-center" style="width: 5%;">Preço Unitário</th>
                                <th class="text-center" style="width: 5%;">Total</th>
                                
                                <!--<th class="text-center" style="width: 10%;">Tipo</th>-->
                                <!--<th class="text-center" style="width: 5%;">Tamanho</th> -->
                               
                              </tr>
                          </thead>
                         

                         <?php

                              $contador = 0;
                              foreach ($itens as $produto)
                              {  
                                  echo'<thead>';
                                  echo '<tr>';
                                    echo'<td>';
                                    echo'<input type="search" class="form-control input-sm" placeholder="" aria-controls="dataTables-example">';
                                    echo'</td>';
                                    
                                   
                                    echo'<td>';
                                    echo'<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled="" value="'.$produto->nome.'">';
                                    echo'</td>';
                                    //echo '<td>'.$produto->preco_compra.'</td>';

                                     echo'<td>';
                                    echo'<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled="" value="'.$produto->quantidade.'">';
                                    echo'</td>';

                                    echo'<td>';
                                    echo'<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled="" value="'.$produto->preco_venda.'">';
                                    echo'</td>';


                                      echo'<td>';
                                    echo'<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled="" value="'.$produto->preco_venda.'">';
                                    echo'</td>';
                                  
                                    //echo '<td>'.$produto->tipo.'</td>'; 
                                    //echo '<td>'.$produto->tamanho.'</td>'; 
                                    
                                    //echo '<td>'.$monitoria->Email.'</td>';
                                    //echo '<td class="text-center">';
                                        
                                        //checkbox presença do aluno
                                        //echo'<input type="checkbox" value="presenca" class="" name="presenca">';
                               
                                      //Botão detalhes
                                      /*echo ' <a href="http://localhost/monitoria/ci/index.php/monitoria/detalhes/'.$monitoria->Matricula.'" title="Detalhes" class="btn btn-info"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>';*/
                                  echo '</td>'; 
                                  echo '</tr>';
                                  
                                    echo'<tr>';
                                    echo'</tr>';
                                    echo'</thead>';
                              $contador++;
                              }
                          ?>

                          <thead>
                              <tr>  
                                <!--<th class="text-center" style="width: 2%;">Código</th>
                                <th class="text-center" style="width: 25%;">Nome</th> -->
                                <!-- <th class="text-center" style="width: 10%;">Preço de compra</th> -->
                                <!-- <th class="text-center" style="width: 10%;">Preço </th> -->
                                <th class="text-center" style="width: 5%;">Total</th>
                                <!--<th class="text-center" style="width: 10%;">Tipo</th>-->
                                <!--<th class="text-center" style="width: 5%;">Tamanho</th> -->
                                <!--<th class="text-center">Presença</th> -->
                              </tr>
                          </thead>

                      </table>


                       <?php
                          
                              {  
                                  echo'<thead>';
                                  echo'<input class="form-control" id="disabledInput" type="text" placeholder="Disabled input" disabled="" value="'.$produto->preco_venda.'">';
                                  echo'</thead>';
                            
                              }
                        ?>


              
                </div>
            
        </div>
    </div>

    </div>

 </form> 


    <!-- jQuery -->
    <script src="/loja/ci/bootstrap/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="/loja/ci/bootstrap/vendor/raphael/raphael.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/morrisjs/morris.min.js"></script>
    <script src="/loja/ci/bootstrap/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/loja/ci/bootstrap/dist/js/sb-admin-2.js"></script>

</body>

</html>
