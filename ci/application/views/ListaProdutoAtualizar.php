<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <link href="/loja/ci/bootstrap/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/morrisjs/morris.css" rel="stylesheet">

    <link href="/loja/ci/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos">Loja de Calçados</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
             
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        
                        <li class="divider"></li>
                        <li><a href="http://localhost/loja/ci/index.php/login/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       <br>
                      <li>
                            <a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos"><i class="fa fa-dashboard fa-fw"></i> Início</a>
                        </li>
                        <li>
                            <a href="http://localhost/loja/ci/index.php/adicionarproduto/add"><i class="fa fa-edit fa-fw"></i> Cadastrar Produto </a>
                        </li>
                        <li>
                          
                            <a href="http://localhost/loja/ci/index.php/paginaeditarproduto/paginaeditar" title="atualizar cadastro"><i class="fa fa fa-edit fa-fw"></i> Atualizar Estoque</a>
                            
                        </li>
                        <li>
                              <a href="http://localhost/loja/ci/index.php/paginaapagarproduto/paginaapagar"><i class="fa fa-edit fa-fw"></i> Apagar Produto</a>
                        </li>
                       
                        <li>
                            <a href="http://localhost/loja/ci/index.php/relatorioproduto/paginaRelatorio"><i class="fa fa-edit fa-fw"></i> Relatórios</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Atualizar Produtos</h1>
                </div>

                 <form action="estoquecompleto" method="post">
                    <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                            <h4>Produtos Cadastrados</h4>
                        </font></font></div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table width="100%" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline collapsed" id=" dataTables-example" role="grid" aria-describedby="dataTables-example_info" style="width: 90%; margin-left: 5%;">
                                    <thead>
                                        <tr>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Código</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nome</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Preço de compra</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Preço de venda</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Quantidade</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tipo</font></font></th>
                                            <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Numeração</font></font></th>
                                              <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ação</font></font></th>
                                        </tr>
                                    </thead>
                                     <?php

                              $contador = 0;
                              foreach ($itens as $produto)
                              {        
                                  echo '<tr>';
                                    echo '<td>'.$produto->id.'</td>';
                                    echo '<td>'.$produto->nome.'</td>';
                                    echo '<td>'.$produto->preco_compra.'</td>';
                                    echo '<td>'.$produto->preco_venda.'</td>'; 
                                    echo '<td>'.$produto->quantidade.'</td>'; 
                                    echo '<td>'.$produto->tipo.'</td>'; 
                                    echo '<td>'.$produto->tamanho.'</td>'; 
                                    
                                    echo '<td>';
                                      echo '<a href="http://localhost/loja/ci/index.php/editarproduto/editar/'.$produto->id.'" title="Editar cadastro" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
                                    echo '</td>';
                                    echo '</td>'; 
                                  echo '</tr>';
                              $contador++;
                              }
                          ?>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                </form> 

                        <div class="col-md-12">
                          Todal de Registro: <?php echo $contador ?>
                        </div>
                      </div>

                    </div>
        </div>
       
    </div>

    <script src="/loja/ci/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/metisMenu/metisMenu.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/raphael/raphael.min.js"></script>
    <script src="/loja/ci/bootstrap/vendor/morrisjs/morris.min.js"></script>
    <script src="/loja/ci/bootstrap/data/morris-data.js"></script>
    <script src="/monitoria/ci/bootstrap/dist/js/sb-admin-2.js"></script>

</body>

</html>
