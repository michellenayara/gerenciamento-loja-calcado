<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AtualizarProduto extends CI_Controller {

public function atualizar(){
		
		if(isset($_SESSION['usuario'])){
				
				if ($this->input->post('nome') == NULL) {
					echo 'O nome do produto é obrigatório.';
					echo '<a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos" title="voltar">Voltar</a>';
				} else{
					if ($this->input->post('tipo') == NULL) {
						echo 'o tipo do produto é obrigatório.';
						echo '<a href="http://localhost/loja/ci/index.php/listagemproduto/listaprodutos" title="voltar">Voltar</a>';
					} else {
								
						$this->load->model('lista_model', 'lista');
						
						$dados['id'] = $this->input->post('codigo');
						$dados['nome'] = $this->input->post('nome');
						$dados['preco_compra'] = $this->input->post('preco_compra');
						$dados['preco_venda'] = $this->input->post('preco_venda');		
						$dados['quantidade'] = $this->input->post('quantidade');
						$dados['tipo'] = $this->input->post('tipo');
						$dados['tamanho'] = $this->input->post('tamanho');		
						
					if ($this->input->post('codigo') != NULL) {		
						$this->lista->editarProduto($dados, $this->input->post('codigo'));

					} else {
						$this->lista->addProduto($dados);
					}
						$this->load->model("lista_model","lista");

						$produto['itens'] = $this->lista->getProdutos();

						$this->load->view('listaprodutoatualizar', $produto);
					}
				}
		}else{
			redirect("http://localhost/monitoria/ci");
		}
	}
}