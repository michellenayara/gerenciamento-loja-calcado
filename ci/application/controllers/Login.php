<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function index()
	{		
		$this->load->model('loja_model', 'loja');
		
		if(isset($_SESSION['usuario'])){
			$this->load->view('login');
		}else{					
			$this->load->view('login');
		}
	}

	public function entrarlogin(){
			
			$this->load->view('login');

			$login = $this->input->post('login');
	        $senha = $this->input->post('password');

	        $this->db->where('nome', $login);
	        $this->db->where('senha', $senha);
		
	        $query = $this->db->get('usuario');

	        if( $query->num_rows() == 1){
					$usuario = $query->row();
					$_SESSION['usuario'] = $usuario->nome;
				
					redirect('http://localhost/loja/ci/index.php/listagemproduto/listaprodutos');
				}else{
					echo '<script>
	                   	alert("Login ou Senha incorretos");
	                 </script>';
				}
			
	}

	public function logout(){

			unset($_SESSION['usuario']);
			
			$this->load->model('lista_model', 'lista');					
			$this->load->view('login');
	}

}
