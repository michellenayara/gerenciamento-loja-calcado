<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListagemProduto extends CI_Controller {

		public function listaProdutos(){

					$this->load->model("lista_model","lista");
						
					if(isset($_SESSION['usuario'])){
						$produtos['itens'] = $this->lista->getProdutos();
						$this->load->view('estoque',$produtos);
					}else{
						redirect('http://localhost/loja/ci');
					}
		}
}
