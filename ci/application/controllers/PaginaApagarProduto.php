
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaginaApagarProduto extends CI_Controller {

	public function paginaapagar(){
		
			$this->load->model("lista_model","lista");

			if(isset($_SESSION['usuario'])){
				$produto['itens'] = $this->lista->getProdutos();
				$this->load->view('listaprodutoapagar', $produto);
			}else{
				redirect('http://localhost/loja/ci');
			}

	}
}