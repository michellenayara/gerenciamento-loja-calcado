<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EditarProduto extends CI_Controller {
	
	public function editar($codigo=NULL){

		if(isset($_SESSION['usuario'])){
			if($codigo == NULL) {
				redirect('http://localhost/loja/ci');
			}

			$this->load->model('lista_model','lista');

			$query = $this->lista->getProdutoByEstoque($codigo);

			if($query == NULL) {
				redirect('http://localhost/loja/ci');
			}
			
			$produtos['produto'] = $query;

			$this->load->view('atualizarestoque', $produtos);
		}else{
			redirect('http://localhost/loja/ci');
		}	
	}
}