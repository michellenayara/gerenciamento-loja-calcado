<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApagarProduto extends CI_Controller {


	public function apagar($codigo=NULL){

		
			if(isset($_SESSION['usuario'])){

				if($codigo == NULL) {
					redirect("http://localhost/loja/ci/index.php/listagemproduto/listaprodutos");
				}

				$this->load->model('lista_model','lista');

				$query = $this->lista->getProdutoByEstoque($codigo);

				if($query != NULL) {
					
					$this->lista->apagarProduto($query->id);
				
				} else {
					
					redirect("http://localhost/loja/ci/index.php/listagemproduto/listaprodutos");
				}

						$this->load->model("lista_model","lista");

						
						$produto['itens'] = $this->lista->getProdutos();

						$this->load->view('listaprodutoapagar', $produto);
			
		}else{
			redirect('http://localhost/loja/ci');
		}
	}
}	