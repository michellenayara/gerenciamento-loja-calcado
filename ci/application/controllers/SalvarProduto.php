<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SalvarProduto extends CI_Controller {


	public function salvar(){
	
		if(isset($_SESSION['usuario'])){
				if ($this->input->post('nome') == NULL) {
					echo 'O nome do produto é obrigatório.';
					echo '<a href="http://localhost/loja/ci/index.php/adicionarproduto/add" title="voltar">Voltar</a>';
				} else{
					if ($this->input->post('preco_compra') == NULL) {
						echo 'O precode compra do produto é obrigatório.';
						echo '<a href="http://localhost/loja/ci/index.php/adicionarproduto/add" title="voltar">Voltar</a>';
					} else {		
						$this->load->model('lista_model', 'lista');
						$dados['id'] = $this->input->post('codigo');
						$dados['nome'] = $this->input->post('nome');
						$dados['preco_compra'] = $this->input->post('preco_compra');
						$dados['preco_venda'] = $this->input->post('preco_venda');		
						$dados['quantidade'] = $this->input->post('quantidade');
						$dados['tipo'] = $this->input->post('tipo');
						$dados['tamanho'] = $this->input->post('tamanho');
						
						$this->lista->addProduto($dados);		
						redirect("http://localhost/loja/ci/index.php/listagemproduto/listaprodutos");	
					}
				}
		}else{
			redirect('http://localhost/loja/ci');
		}
	}
}