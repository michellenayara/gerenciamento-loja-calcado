<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RelatorioProduto extends CI_Controller {

	public function paginaRelatorio(){
		
			$this->load->model("lista_model","lista");

			if(isset($_SESSION['usuario'])){
				
				$produto['itens'] = $this->lista->getProdutos();

				$this->load->view('relatorioestoque', $produto);
			}else{
				redirect('http://localhost/loja/ci');
			}

	}
}