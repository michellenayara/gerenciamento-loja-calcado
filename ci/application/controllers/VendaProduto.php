<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class VendaProduto extends CI_Controller {

	public function venda(){

		$this->load->model('lista_model', 'lista');

		if(isset($_SESSION['usuario'])){

				//Buscar dados no banco
				$produto['itens'] = $this->lista->getProdutos();

				//Passar dados do banco para view
				$this->load->view('telavenda', $produto);
			}else{
				//Fazemos um redicionamento para a página 		
				redirect("http://localhost/loja/ci");
			}
	}

}