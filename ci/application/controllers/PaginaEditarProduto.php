<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaginaEditarProduto extends CI_Controller {

		public function paginaeditar(){
		
			$this->load->model("lista_model","lista");
			
			if(isset($_SESSION['usuario'])){
				
				$produto['itens'] = $this->lista->getProdutos();

				$this->load->view('listaprodutoatualizar', $produto);
			}else{		
				redirect("http://localhost/loja/ci");
			}
		}
}