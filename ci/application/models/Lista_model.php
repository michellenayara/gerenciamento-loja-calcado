<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Lista_model extends CI_Model {

	public function getProdutos(){

			$query = $this->db->get('estoque');
			return $query->result();
	}

	//Adiiona produtos na tabela
	public function addProduto($dados=NULL){

    	if ($dados != NULL):
    		$this->db->insert('estoque', $dados);		
        endif;
    }

    //Atualiza um produto na tabela produtos
    public function editarProduto($dados=NULL, $codigo=NULL){
        //Verifica se foi passado $dados e $id    
        if ($dados != NULL && $codigo != NULL):
            //Se foi passado ele vai a atualização
            $this->db->update('estoque', $dados, array('id'=>$codigo));      
        endif;
    } 

    //Get produtos by id
    public function getProdutoByEstoque( $codigo = NULL){
        
        if ($codigo != NULL):
            //Verifica se a ID no banco de dados
            $this->db->where('id', $codigo);        
            //limita para apenas um regstro    
            $this->db->limit(1);
            //pega os produto
            $query = $this->db->get("estoque");        
            //retornamos o produto
            return $query->row();   
        endif;
    }

    //Apaga um produtos na tabela produtos 
    public function apagarProduto($codigo = NULL){
        //Verificamos se foi passado o a ID como parametro
        if ($codigo!= NULL):
            //Executa a função DB DELETE para apagar o produto
            $this->db->delete('estoque', array('id'=>$codigo));            
        endif;
    }  

}